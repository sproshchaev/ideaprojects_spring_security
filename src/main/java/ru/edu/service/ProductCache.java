package ru.edu.service;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component // синглтон - будет доступен в единичном варианте
public class ProductCache {

    private final Map<String, Product> cache = new HashMap<>();

    // Создание CRUD-репозитория
    public List<Product> getAll() {
        return new ArrayList<>(cache.values());
    }

    // Получение товара по id
    public Product get(String id) {
        return cache.get(id);
    }

    // Создание товара
    public Product create(Product info) {
        cache.put(info.getId(), info);
        return info;
    }

    // Изменение товара
    public Product update(Product info) {
        cache.put(info.getId(), info);
        return info;
    }

    // Удаление товара
    public Product remove(String id) {
        return cache.remove(id);
    }


}
