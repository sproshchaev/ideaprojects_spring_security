package ru.edu.service;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class UserCache {

    private Map<String, UserInfo> cache = new HashMap<>();

    public UserInfo get(String id) {
        return cache.get(id);
    }

    private void create(UserInfo info) {
        cache.put(info.getLogin(), info);
    }

    @PostConstruct
    public void init() {
        // Администраторы
        create(new UserInfo("anton777", "qwerty", "Anton Trenkunov", "ADMIN"));
        create(new UserInfo("anton778", "qwerty2", "Anton Ivanov", "ADMIN"));
        // Пользователь
        create(new UserInfo("anton779", "qwerty3", "Denis Ivanov", "MANAGER"));

    }

}
