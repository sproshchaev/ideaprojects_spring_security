package ru.edu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.service.Product;
import ru.edu.service.ProductCache;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {

    private ProductCache cache;

    // http://localhost:8080/admin/create
    @GetMapping("/create")
    public ModelAndView createProductView(HttpServletRequest request, HttpServletResponse response) throws IOException {


        return  new ModelAndView("/create_product.jsp");
    }

    @PostMapping("/update")
    public ModelAndView updateProduct(HttpServletRequest request, HttpServletResponse response,
                                      @RequestParam("id") String id,
                                      @RequestParam("name") String name,
                                      @RequestParam("price") String price) throws IOException {



        Product info = new Product(id, name, Double.parseDouble(price));
        cache.update(info);

        // 32:56 Перенаправление на новую страницу с инфо, что продукт был создан
        ModelAndView view = new ModelAndView("/sucsess_create_product.jsp");

        return view;


    }




    @Autowired
    public void setCache(ProductCache cache) {
        this.cache = cache;
    }

}
