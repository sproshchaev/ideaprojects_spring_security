package ru.edu.controller;

// Создание класса AuthResult, хранящего результат авторизации
public class AuthResult {

    private String status;

    public AuthResult(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "AuthResult{" +
                "status='" + status + '\'' +
                '}';
    }
}
