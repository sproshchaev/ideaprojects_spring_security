package ru.edu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping(value = "/info")
public class InfoController {

    // Запрос http://localhost:8080/info/ и http://localhost:8080/info
    @GetMapping
    public ModelAndView info() {

        // Вариант в лекции
        //ModelAndView modelAndView = new ModelAndView();
        //modelAndView.setViewName("/info.jsp");
        //modelAndView.addObject("author", "Sergey Proshchaev");
        //modelAndView.addObject("title", "Custom secured app");

        ModelAndView modelAndView = new ModelAndView("/info.jsp");
        modelAndView.addObject("author", "Sergey Proshchaev");
        modelAndView.addObject("title", "Custom secured app");
        modelAndView.addObject("dateAndTime", new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()));


        return modelAndView;
    }

}
