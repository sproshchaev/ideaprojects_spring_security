package ru.edu.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

// 01:55:33 - конфигурация пользователей. Добавление класса конфигурации MySecurityConfig
@Configuration
@EnableWebSecurity
public class MySecurityConfig extends WebSecurityConfigurerAdapter {

    // 02:08:06 - переопределение метода configure
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //http.authorizeRequests()
        //        .antMatchers("/info").permitAll()
        //        .antMatchers("/market").permitAll()
        //        .anyRequest().authenticated()
        //        .and()
        //        .formLogin();

        // 02:13:15 - отключение защиты CSRF
        http.csrf().ignoringAntMatchers("/admin/update")
                .and()
                .authorizeRequests()
                .antMatchers("/info").permitAll()
                .antMatchers("/market").permitAll()
                .antMatchers("/admin/*").hasAnyRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .and()
                .exceptionHandling().accessDeniedPage("/access_denied.jsp");

    }

    // вид обработки пароля - сравнение построчно паролей
    @Bean
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }


}
